<?php
/**
 * Created by abu.
 * Function: 基类控制器
 * Date: 2018/4/10
 * Time: 下午3:39
 */

namespace app\admin\controller;

use think\Controller;

class Base extends Controller
{
    public $account;
    public function _initialize()
    {
        // 登陆判断
        $isLogin = $this->isLogin();
        if(!$isLogin)
        {
            return $this->redirect(url('/login'));
        }
    }

    // 判断是否登陆
    public function isLogin()
    {
        // 获取session
        $user = $this->getLoginUser();
        if($user && $user->id)
        {
            return true;
        }
        return false;
    }

    public function getLoginUser()
    {
        if(!$this->account)
        {
            $this->account = session('adminAccount','','admin');
        }
        return $this->account;
    }
}