<?php
/**
 * Created by abu.
 * Function: 登陆模块
 * Date: 2018/4/10
 * Time: 下午2:55
 */

namespace app\admin\controller;

use think\Controller;

class Login extends Controller
{
    public function index()
    {
        if(request()->isPost())
        {
            // 登陆逻辑
            $data = input('post.');
            // 通过用户名 获取 用户相关信息
            $res = model('UserList')->get(['username'=>$data['username']]);

            if(!$res)
            {
                $this->error('该用户不存在');
            }

            if($res->password != md5($data['password']))
            {
                $this->error('密码错误');
            }

            model('UserList')->updateById(['last_login_time'=>time()],
                $res->id);

            // session admin 是作用域
            session('adminAccount',$res,'admin');

            return $this->redirect(url('/home'));
        }
        else
        {
            // 获取session
            $account = session('adminAccount','','admin');
            if($account && $account->id)
            {
                return $this->redirect(url('/home'));
            }
            else
            {
                return $this->fetch();
            }
        }
    }

    public function logout()
    {
        // 清除 session
        session(null, 'admin');
        // 跳转
        $this->redirect('/login');
    }
}