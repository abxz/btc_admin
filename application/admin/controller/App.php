<?php
namespace app\admin\controller;

use think\Controller;

class App extends Base
{
    /**
     * 列表页
     */
    public function index()
    {
        $data = input('post.');

        $appList = model('AppList')->getList($data);
        $typeList = model('AppType')->getAll();
        $typeDic = array_column($typeList,'type_name','type_id');

        foreach ($appList as $k => $v)
        {
            $appList[$k]['type_name'] = $typeDic[$v['type_id']];
        }

        return $this->fetch('',[
            'id' => input('param.id'),
            'appList' => $appList,
            'typeList' => $typeList,
        ]);
    }

    /**
     * app添加
     */
    public function add()
    {
        $typeList = model('AppType')->getList();
        return $this->fetch('',[
            'id' => input('param.id'),
            'typeList' => $typeList
        ]);
    }

    /**
     * 排序
     */
    public function listorder($id, $orderId)
    {
        $res = model('AppList')->save(['order_id'=>$orderId], ['app_id'=>$id]);

        if($res)
        {
            echo json_encode(['status'=>1, 'info'=>'操作成功']);
        }
        else
        {
            echo json_encode(['status'=>0, 'info'=>'操作失败，请重试']);
        }
    }

    /**
     * 上传图片
     */
    public function upload()
    {
        $pathName = $this->request->param('path');//图片存放的目录
        $file = request()->file('file');
        $path =  'static/uploads/images/' . (!empty($pathName) ? $pathName : 'img');
        if(!is_dir($path))
        {
            mkdir($path, 0755, true);
        }
        $info = $file->move($path);
        if ($info && $info->getPathname())
        {
            $iconPath =  '/'.$info->getPathname();
            $data = [
                'status' => 1,
                'data' =>  $iconPath,
            ];
            // 数据填充
            $res = model('IconList')->add(['icon_src'=>$iconPath]);
            if($res)
            {
                echo exit(json_encode($data));
            }
            else
            {
                echo exit(json_encode($res));
            }
        }
        else
        {
            echo exit(json_encode($file->getError()));
        }
    }

    /**
     * app动作（添加、编辑）
     */
    public function save()
    {
        // 严格判定
        if(!request()->isPost())
        {
            $this->error('请求失败');
        }
        // 接受数据
        $data = request()->post();
        // 去除'file'（上传图片后，不知道哪里来的file,待查处）
        unset($data['file']);

        // validate数据校验
        $validate = validate('Category');
        if(!$validate->scene('add')->check($data))
        {
            $this->error($validate->getError());
        }

        // 编辑
        if(!empty($data['app_id']))
        {
            return $this->update($data);
        }

        // 防重校验
        $ret = model('AppList')->getInfo($data);
        if($ret != null)
        {
            $this->error('已添加该app，请勿重复添加');
        }

        // 数据填充
        $res = model('AppList')->add($data);
        if($res)
        {
            $this->redirect('/app/list');
        }
        else
        {
            $this->error('新增失败');
        }
    }

    /**
     * app编辑
     */
    public function edit($app_id=0)
    {
        if(intval($app_id) < 1) {
            $this->error('参数不合法');
        }

        $app = model('AppList')->get($app_id);

        $typeList = model('AppType')->getList();

        return $this->fetch('',[
            'app' => $app,
            'typeList' => $typeList
        ]);
    }

    /**
     * app更新
     */
    public function update($data) {
        $res = model('AppList')->save($data, ['app_id' => intval($data['app_id'])]);
        if($res)
        {
            $this->redirect('/app/list');
        }
        else
        {
            $this->error('编辑失败');
        }
    }

    /**
     * 分类添加
     */
    public function type()
    {
        if(request()->isPost())
        {
            $data = request()->post();

            // validate数据校验
            $validate = validate('Category');
            if(!$validate->scene('type')->check($data))
            {
                $this->error($validate->getError());
            }

            // 数据填充
            $res = model('AppType')->add($data);
            if($res)
            {
                $this->success('新增成功');
            }
            else
            {
                $this->error('新增失败');
            }
        }
        else
        {
            $typeList = model('AppType')->getList();

            return $this->fetch('',[
                'id' => input('param.id'),
                'typeList' => $typeList,
            ]);
        }
    }

    /**
     * 分类列表
     */
    public function typeList()
    {
        $typeList = model('AppType')->getAll();

        return $this->fetch('',[
            'typeList' => $typeList,
        ]);
    }
}
