<?php
namespace app\admin\controller;

class Index extends Base
{
    public function index()
    {
        $count = model('AppList')->count();                             // App总数
        $dayCount = count(model('AppCount')->getList(['day'=>1]));      // 当日点击
        $clickOrder = model('AppList')->getOrder();                     // App总数

        // 日流量统计
        $dayFlow = model('WebFlow')->getFlowChart();
        foreach ($dayFlow as $k=>$v)
        {
            $dayFlow[$k]['date_time'] = date('Y-m-d',$v['date_time']);
        }
        $flow = json_encode($dayFlow);

        return $this->fetch('',[
            'id' => input('param.id'),
            'appCount' => $count,
            'dayCount' => $dayCount,
            'clickOrder' => $clickOrder,
            'flow' => $flow,
        ]);
    }
}
