<?php
/**
 * Created by abu
 * Function: 数据规则校验函数类
 * Date: 2018/4/9
 * Time: 上午10:56
 */
namespace app\admin\validate;
use think\Validate;
class Category extends Validate {
    protected $rule = [
        ['icon_src', 'require', '图片不能为空'],
        ['type_id', 'require', '类型不能为空'],
        ['name', 'require|max:20', 'app名称不能为空|名称不能超过20字符'],
        ['official_url', 'require', '官方地址不能为空'],
        ['app_id', 'number'],
        ['status', 'number|in:-1,0,1','状态必须是数字|状态范围不合法'],
        ['type_name', 'require', '分类名称不能为空'],
        ['listorder', 'number'],
    ];

    /** 场景分类 */
    protected $scene = [
        'add' => ['icon_src', 'type_id', 'name', 'official_url', 'app_id'],   // app添加
        'type' => ['type_name'],                                              // 类别添加
        'listorder' => ['id', 'listorder'],                                   // 排序
    ];
}