<?php
/**
 * Created by abu.
 * Function: 网站流量统计
 * Date: 2018/4/9
 * Time: 下午6:22
 */
namespace app\common\model;
use think\Model;
class WebFlow extends Model
{
    public function add($data)
    {
        $data['date_time'] = mktime(0,0,0,date('m'),date('d'),date('Y'));
        $data['create_time'] = time();
        $result = $this->save($data);
        return $result;
    }

    public function getList($data)
    {
        $where = [];
        if(!empty($data['day']))
        {
            $where['date_time'] = mktime(0,0,0,date('m'),date('d'),date('Y'));
        }

        $result = $this->where($where)->select();
        return $result;
    }

    /**
     * 获取日流量统计
     */
    public function getFlowChart()
    {
        $result = $this->query("SELECT `date_time`,count(id) as day_flow FROM `web_flow` GROUP BY `date_time` ORDER BY date_time asc");
        return $result;
    }
}