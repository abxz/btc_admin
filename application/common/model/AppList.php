<?php
namespace app\common\model;

use think\Model;

class AppList extends Model
{
    protected $autoWriteTimestamp = true;

    public function getInfo($data)
    {
        $where = [];
        if(!empty($data['name']))
        {
            $where['name'] = trim($data['name']);
        }

        $result = $this->where($where)->find();
        return $result;
    }

    public function add($data)
    {
        $data['status'] = 1;
        $result = $this->save($data);
        return $result;
    }

    public function getList($data)
    {
        $where = [];
        if(!empty($data['name']))
        {
            $where['name'] = ['like','%'.$data["name"].'%'];
        }

        if(!empty($data['type_id']))
        {
            $where['type_id'] = $data['type_id'];
        }

        $order = [
            'order_id' => 'desc',
            'app_id' => 'desc',
        ];

        $result = $this->where($where)->order($order)->paginate(10);
//        echo $this->getlastsql();
        return $result;
    }

    public function getOrder()
    {
        $where = [];
        if(!empty($data['name']))
        {
            $where['name'] = ['like','%'.$data["name"].'%'];
        }

        if(!empty($data['type_id']))
        {
            $where['type_id'] = $data['type_id'];
        }

        $order = [
            'count_time' => 'desc',
        ];

        $result = $this->where($where)->order($order)->limit(4)->select();
        return $result;
    }

    public function getAll($data)
    {
        $order = [
            'order_id' => 'desc',
            'app_id' => 'asc',
        ];
        $data['status'] = 1;

        $result = $this->where($data)->order($order)->select();
        return $result;
    }

    public function getCount()
    {
        $count = $this->count();
        return $count;
    }

    public function updateCountTime($data)
    {
        $result = $this->where($data)->update(['count_time' => ['exp','count_time+1'],]);
        return $result;
    }
}