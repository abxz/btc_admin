<?php
/**
 * Created by abu.
 * Function: 后台登陆
 * Date: 2018/4/10
 * Time: 下午3:12
 */

namespace app\common\model;

use think\Model;

class UserList extends Model
{
    public function updateById($data, $id)
    {
        // allowField 过滤data数组中非数据表中的数据
        return $this->allowField(true)->save($data, ['id'=>$id]);
    }
}