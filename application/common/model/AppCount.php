<?php
/**
 * Created by abu.
 * Function: 前端页面点击统计
 * Date: 2018/4/9
 * Time: 下午6:22
 */
namespace app\common\model;
use think\Model;
class AppCount extends Model
{
    public function add($data)
    {
        $data['date_time'] = mktime(0,0,0,date('m'),date('d'),date('Y'));
        $data['create_time'] = time();
        $result = $this->save($data);
        return $result;
    }

    public function getList($data)
    {
        $where = [];
        if(!empty($data['day']))
        {
            $where['date_time'] = mktime(0,0,0,date('m'),date('d'),date('Y'));
        }

        $result = $this->where($where)->select();
        return $result;
    }

    /**
     * 获取日流量统计
     */
    public function getFlowChart()
    {
//        $result = $this->field('date_time,count(id) as day_flow')
//            ->group('date_time')
//            ->order('date_time asc')
//            ->select();
        $result = $this->query("SELECT `date_time`,count(id) as day_flow FROM `app_count` GROUP BY `date_time` ORDER BY date_time asc");
        return $result;
    }
}