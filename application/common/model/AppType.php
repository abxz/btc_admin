<?php
/**
 * Created by abu.
 * Function: app类别
 * Date: 2018/4/9
 * Time: 下午4:38
 */

namespace app\common\model;

use think\Model;

class AppType extends Model
{
    protected $autoWriteTimestamp = true;

    public function add($data)
    {
        $result = $this->save($data);
        return $result;
    }

    public function getList()
    {
        $order = [
            'type_id' => 'desc',
        ];

        $result = $this->order($order)->paginate(100);
        return $result;
    }

    public function getAll()
    {
        $where['status'] = 1;
        $result = $this->where($where)->select();
        return $result;
    }
}