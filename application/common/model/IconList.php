<?php
/**
 * Created by abu.
 * Function: 图片上传
 * Date: 2018/4/10
 * Time: 上午11:02
 */

namespace app\common\model;

use think\Model;

class IconList extends Model
{
    public function add($data) {
        $data['create_time'] = time();
        $result = $this->save($data);
        return $result;
    }
}