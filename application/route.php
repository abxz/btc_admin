<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

return [
    '__pattern__' => [
        'name' => '\w+',
    ],
    '[hello]'     => [
        ':id'   => ['index/hello', ['method' => 'get'], ['id' => '\d+']],
        ':name' => ['index/hello', ['method' => 'post']],
    ],
    // 登陆页
    '[login]'     => [
        ''   => 'admin/login/index',
        'out'   => 'admin/login/logout',
    ],
    // 主页
    '[home]'     => [
        ''   => 'admin/index/index',
    ],
    // app管理
    '[app]'     => [
        'list'   => 'admin/app/index',
        'add'    => 'admin/app/add',
        'edit/:app_id'    => 'admin/app/edit',
        'type'   => 'admin/app/type',
        'save'   => 'admin/app/save',
    ],
];
