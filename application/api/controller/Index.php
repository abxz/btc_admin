<?php
namespace app\api\controller;

use think\Controller;
use think\Request;

class Index extends Controller
{
    public function index()
    {
        // 指定允许其他域名访问
        header('Access-Control-Allow-Origin:*');
        // 响应类型
        header('Access-Control-Allow-Methods:POST');
        // 响应头设置
        header('Access-Control-Allow-Headers:x-requested-with,content-type');

        $data = [];
        $typeList = model('AppType')->getAll();
        foreach ($typeList as $k=>$v)
        {
            $appList = model('AppList')->getAll(['type_id'=>$v['type_id']]);

            // 类型下无内容不予以显示
            if(empty($appList))
            {
                continue;
            }

            $apps = [];
            $data[$k]['title'] = $v['type_name'];
            foreach ($appList as $key=>$value)
            {
                $apps[$key]['id'] = $value['app_id'];
                $apps[$key]['name'] = $value['name'];
                $apps[$key]['src'] = $value['official_url'];
                $apps[$key]['imgSrc'] = $value['icon_src'];
                $apps[$key]['download']['android'] = $value['android_url'];
                $apps[$key]['download']['ios'] = $value['iphone_url'];
            }
            $data[$k]['apps'] = $apps;
        }

        echo json_encode($data);
    }

    /**
     * 统计点击次数
     */
    public function count()
    {
        // 指定允许其他域名访问
        header('Access-Control-Allow-Origin:*');
        // 响应类型
        header('Access-Control-Allow-Methods:POST');
        // 响应头设置
        header('Access-Control-Allow-Headers:x-requested-with,content-type');

        $data = input('get.');
        $ip = sprintf("%u\n", ip2long(Request::instance()->ip()));

        $res = model('AppCount')->add(['app_id'=>$data['id'],'ip'=>$ip]);

        if($res)
        {
            $ret = model('AppList')->updateCountTime(['app_id'=>$data['id']]);

            if($ret)
            {
                $data = ['status'=>1,'info'=>'请求成功'];
            }
        }
        else
        {
            $data = ['status'=>0,'info'=>'请求失败'];
        }

        echo json_encode($data);
    }

    /**
     * 统计网站访问量
     */
    public function webFlow()
    {
        // 指定允许其他域名访问
        header('Access-Control-Allow-Origin:*');
        // 响应类型
        header('Access-Control-Allow-Methods:POST');
        // 响应头设置
        header('Access-Control-Allow-Headers:x-requested-with,content-type');

        $ip = sprintf("%u\n", ip2long(Request::instance()->ip()));

        $res = model('WebFlow')->add(['ip'=>$ip]);

        if($res)
        {
            $data = ['status'=>1,'info'=>'请求成功'];
        }
        else
        {
            $data = ['status'=>0,'info'=>'请求失败'];
        }

        echo json_encode($data);
    }
}
